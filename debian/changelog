meep-mpich2 (1.7.0-3) sid; urgency=medium

  * debian/*postinst debian/*prerm: add correct package name
              (Closes: #918074, #918858)
              Thanks to Peter Green and Andreas Beckmann for their hints
  * debian/control: update description according to upstream wishes
  * debian/control: update homepage
  * debian/control: Standards Version to 4.3.0. (No changes needed).
  * debian/copyright: update Upstream-Source:

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 04 Feb 2019 19:00:00 +0100

meep-mpich2 (1.7.0-2) unstable; urgency=medium

  * upload to unstable

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 25 Dec 2018 16:29:02 +0100

meep-mpich2 (1.7.0-1) experimental; urgency=medium

  * New upstream release
  * debian/control: use dh11
  * debian/control: Standards Version to 4.2.1. (No changes needed).
  * debian/control: use libctl>=4.1.4-2
  * debian/control: add swig dependency
  * debian/control: remove dependencies: autotools-dev, dh-autoreconf
  * debian/rules: add --enable-maintainer-mode to configure flags
  * debian/control: depend on newest libctl and thus on guile2.2
                    (Closes: #885217)
  * debian/control: depend on newest libharminv
  * debian/control: add salsa VCS URLs
  * debian/control: bump SONAME to 12
  * debian/control: upstream does not support parallel installation of
                    serial and MPI version any longer
                    add conflicts accordingly
  * debian/rules: add --enable-maintainer-mode to configure flags
  * debian/rules: enable hardening flags
  * update debian/watch

  [ Ruben Undheim ]
  * add python package

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 22 Dec 2018 19:29:02 +0100

meep-mpich2 (1.3-4) unstable; urgency=medium

  * rebuild with gcc-6 (Closes: #831106)
  * debian/control: bump standard to 3.9.8, no changes

 -- Thorsten Alteholz <debian@alteholz.de>  Tue, 09 Aug 2016 14:10:00 +0200

meep-mpich2 (1.3-3) unstable; urgency=medium

  * Update build dependencies for GSL 2, change libgsl0-dev to libgsl-dev.
    Thanks to Bas Couwenberg for the patch. (Closes: #807214)
  * debian/rules: put correct path to --with-libctl
  * builds again after the mpich transition (Closes: #807747)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 27 Dec 2015 13:30:00 +0100

meep-mpich2 (1.3-2) unstable; urgency=medium

  * debian/control: add conflict with old version of libs (Closes: #790747)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 05 Jul 2015 09:20:00 +0200

meep-mpich2 (1.3-1) unstable; urgency=medium

  * new upstream version
  * soname changed to 8
  * debian/control: Standards Version to 3.9.6. (No changes needed).

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 21 Jun 2015 15:30:00 +0200

meep-mpich2 (1.2.1-3) unstable; urgency=medium

  * Support hdf5 1.8.13 new packaging layout. (Closes: #756681)
    (patch by Gilles Filippini <pini@debian.org>)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 03 Aug 2014 14:30:00 +0200

meep-mpich2 (1.2.1-2) unstable; urgency=medium

  * debian/control: add Conflicts: with old lib (Closes: #748660)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 25 May 2014 19:00:00 +0200

meep-mpich2 (1.2.1-1) unstable; urgency=medium

  * new upstream version
  * soname changed to 7
  * debian/rules: upstream ChangeLog is now called NEWS
  * debian/control: Standards Version to 3.9.5. (No changes needed).
  * debian/control: switch to dh 9
  * debian/control: use guile 2.0 now (Closes: #747800)
  * debian/control: remove DM-Upload-Allowed:
  * debian/control: rebuild against new mpich (Closes: #744116)
  * debian/rules: use debhelper
  * debian/rules: use --with autoreconf (Closes:  #744463)
  * debian/copyright: adapt years

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 11 May 2014 11:20:00 +0200

meep-mpich2 (1.1.1-10) unstable; urgency=low

  * debian/rules: mv /usr/include/meep-mpich2 to /usr/include/meep
                  (Closes: #711768)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 09 Jun 2013 11:00:00 +0200

meep-mpich2 (1.1.1-9) unstable; urgency=low

  * debian/control: add more Conflicts:
  * debian/control: fix typo in mpich2 dependency

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 28 Jun 2012 20:00:00 +0200

meep-mpich2 (1.1.1-8) unstable; urgency=low

  * this package is a spin-off of meep-openmpi (1.1.1-8)
    As requested by users this package is linked against mpich2
    instead of openmpi
  * debian/control: depend on debhelper >=9
  * debian/control: standards version changed to 3.9.3 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 06 Jun 2012 18:00:00 +0200
